package com.me.hickup;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Pool;

public class Fire {
	private Pool<Rectangle> rectPool = new Pool<Rectangle>() {
		@Override
		protected Rectangle newObject () {
			return new Rectangle();
		}
	};
	int x;
	int y;
	int scale;
	Rectangle fire = null;
	
	public Fire(int x, int y, int scale){
		this.x = x;
		this.y = y;
		this.scale = scale;
		
	}
	
	public Rectangle getFireRect(){
		fire = rectPool.obtain();
		fire.set(x, y, scale, scale);
		return fire;
		
	}

}
