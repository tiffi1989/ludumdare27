package com.me.hickup;

import com.badlogic.gdx.math.Vector2;


public class Player {

	static float WIDTH;
	static float HEIGHT;
	static float MAX_VELOCITY = 8.3f;
	static float JUMP_VELOCITY = 40f;
	static float DAMPING = 0.87f;

	enum State {
		Standing,
		Walking,
		Jumping
	}

	final Vector2 position = new Vector2();
	final Vector2 velocity = new Vector2();
	State state = State.Walking;
	float stateTime = 0;
	boolean facesRight = true;
	boolean grounded = false;
	boolean dead = false;
}
