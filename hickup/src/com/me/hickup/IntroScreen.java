package com.me.hickup;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;

public class IntroScreen implements Screen {

	final Hiccup game;
	private TiledMap map;
	private OrthogonalTiledMapRenderer renderer;
	private OrthographicCamera camera;
	private BitmapFont font;
	TiledMapRenderer tiledMapRenderer;
	SpriteBatch spriteBatch;
	
	Sound swallow;

	Animation stand;
	Texture standSheet;
	TextureRegion[] standFrames;
	float lastTime;
	Texture flux;
	int key;
	boolean soundPlayed;

	public IntroScreen(Hiccup game) {
		this.game = game;
		
		swallow = Gdx.audio.newSound(Gdx.files.internal("data/sounds/swallow.wav"));

		map = new TmxMapLoader().load("data/world/level1/level0.tmx");
		renderer = new OrthogonalTiledMapRenderer(map, 1 / 16f);
		renderer = new OrthogonalTiledMapRenderer(map, 1 / 16f);

		flux = new Texture(Gdx.files.internal("images/flux.png"));

		standSheet = new Texture(Gdx.files.internal("images/standing.png"));
		TextureRegion[][] tmp1 = TextureRegion.split(standSheet,
				standSheet.getWidth() / 2, standSheet.getHeight() / 1);
		standFrames = new TextureRegion[2 * 1];
		int index1 = 0;
		for (int i = 0; i < 1; i++)
			for (int j = 0; j < 2; j++) {
				standFrames[index1++] = tmp1[i][j];
			}
		stand = new Animation(0.4f, standFrames);
		spriteBatch = new SpriteBatch();

		camera = new OrthographicCamera();
		camera.setToOrtho(false, 15, 10);

		camera.update();

	}

	@Override
	public void render(float delta) {

		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		TextureRegion frame = null;
		lastTime += Gdx.graphics.getDeltaTime();
		renderer.setView(camera);
		renderer.render();

		renderPlayer(delta);

		camera.update();
		frame = stand.getKeyFrame(lastTime, true);

		spriteBatch.begin();
		spriteBatch.draw(frame, 400, 48, 128, 128);
		if (lastTime <= 12)
			spriteBatch.draw(flux, 550, 140, 64, 64);
		spriteBatch.end();

		key = (int) lastTime / 2;

		game.batch.begin();

		switch (key) {
		case 1:
			game.font.draw(game.batch,
					"Finally, my Tux-Compensator is complete",
					Gdx.graphics.getWidth() / 2, 350);
			break;
		case 2:
			game.font.draw(game.batch, "Police!!! OPEN THE DOOR!", 100, 200);
			break;
		case 3:
			game.font.draw(game.batch, "F*&K!", Gdx.graphics.getWidth() / 2,
					350);
			break;
		case 4:
			game.font.draw(game.batch, "Timetravel is illegal!! ", 100, 200);
			break;
		case 5:
			game.font.draw(game.batch,
					"There is only one logical course of action!",
					Gdx.graphics.getWidth() / 2, 350);
			break;
		case 6:
			game.font.draw(game.batch, "*Swallow..*",
					Gdx.graphics.getWidth() / 2, 350);
			break;
		case 7:
			game.font.draw(game.batch, "...", Gdx.graphics.getWidth() / 2, 350);
			break;
		case 8:
			game.font.draw(game.batch, "Hicks...", Gdx.graphics.getWidth() / 2,
					350);
			break;

		default:
			break;
		}

		game.batch.end();
		
		if(key == 6 && !soundPlayed){
			swallow.play();
			soundPlayed = true;
			
		}
		
		if (lastTime >= 17) {
			game.setScreen(new DefaultScreen(game));
			dispose();
		}

		

	}

	private void renderPlayer(float deltaTime) {
		TextureRegion frame = null;

		frame = stand.getKeyFrame(lastTime);

		SpriteBatch batch = renderer.getSpriteBatch();
		batch.begin();

		batch.draw(frame, 5, 1, Player.WIDTH, Player.HEIGHT);

		batch.end();
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {
		// TODO Auto-generated method stub

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

}
