package com.me.hickup;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;



public class DefaultScreen implements Screen{

	final Hiccup game;
	
	private TiledMap map;
	private OrthogonalTiledMapRenderer renderer;
	private OrthographicCamera camera;
	private BitmapFont font;	
	
	private Pool<Rectangle> rectPool = new Pool<Rectangle>() {
		@Override
		protected Rectangle newObject () {
			return new Rectangle();
		}
	};
	int key = 1;
	Sound timeSound;
	Sound dinoSound;
	Sound deathSound;
	Sound jumpSound;
	float endTime;
	boolean firstTime;
	boolean firstTime2;
	boolean dinoAuto;
	boolean enemyJump;
	Enemy endGegner;
	Enemy gegner;
	Enemy feuer;
	AssetManager assetManager;
	float hicsTime = 0;
	boolean hics;
	boolean level1;
	float lastTime;
	Texture heartSheet;
	Animation heart;
	TextureRegion[] heartFrames;
	Texture fireSheet;
	Animation fire;
	TextureRegion[] fireFrames;
	Texture dinolineSheet;
	Animation dinoline;
	TextureRegion[] dinolineFrames;
	Texture dinoSheet;
	Animation dino;
	TextureRegion[] dinoFrames;
	Animation stand;
	Texture standSheet;
	TextureRegion[] standFrames;
	Animation jump;
	Texture jumpSheet;
	TextureRegion[] jumpFrames;
	Player spieler;
	Animation walk;
	Texture walkSheet;
	TextureRegion[] walkFrames;
	SpriteBatch spriteBatch;
	TextureRegion currentFrame;
	private Array<Rectangle> tiles = new Array<Rectangle>();
	public Rectangle playerRectangle;
	ArrayList<Rectangle> fireRect = new ArrayList<Rectangle>();
	Fire a = new Fire(104, 3, 1);
	Fire b = new Fire(109, 3, 1);
	Fire c = new Fire(114, 3, 1);
	Fire d = new Fire(121, 3, 1);
	Fire e = new Fire(140, 2, 1);
	Fire f = new Fire(141, 2, 1);
	Fire g = new Fire(142, 2, 1);
	Fire i = new Fire(157, 2, 1);
	Fire j = new Fire(160, 2, 1);
	Fire k = new Fire(161, 2, 1);
	Fire m = new Fire(167, 2, 1);
	Fire l = new Fire(182, 6, 1);
	Fire n = new Fire(204, 3, 1);
	Fire o = new Fire(228, 7, 1);
	Fire p = new Fire(238, 3, 1);
	Fire q = new Fire(250, 5, 2);
	Fire r = new Fire(276, 3, 1);
//	Fire s = new Fire(289, 5, 1);
	Fire t = new Fire(313, 8, 8);
	Fire u = new Fire(340, 1, 2);
	Fire v = new Fire(381, 4, 2);
	Fire w = new Fire(390, 4, 1);
	Fire x = new Fire(402, 4, 2);
//	Fire y = new Fire(167, 2, 1);
//	Fire z = new Fire(167, 2, 1);
	
	
	
	private static final float gravity = -2.5f;
	
	float stateTime;
	
	TiledMapRenderer tiledMapRenderer;
	
	
	public DefaultScreen(final Hiccup game){
		this.game = game;
		
		jumpSound = Gdx.audio.newSound(Gdx.files.internal("data/sounds/jump.wav"));
		dinoSound = Gdx.audio.newSound(Gdx.files.internal("data/sounds/dino.wav"));
		deathSound = Gdx.audio.newSound(Gdx.files.internal("data/sounds/death.wav"));
		timeSound = Gdx.audio.newSound(Gdx.files.internal("data/sounds/time.wav"));
		
		
		
		walkSheet = new Texture(Gdx.files.internal("images/walking.png"));
		TextureRegion[][] tmp = TextureRegion.split(walkSheet, walkSheet.getWidth() / 4, walkSheet.getHeight() / 1);
		walkFrames = new TextureRegion[4 * 1];
		int index = 0;
		for(int i = 0; i < 1; i++)
			for(int j = 0; j < 4; j++){
				walkFrames[index++] = tmp[i][j];
			}
		walk = new Animation(0.1f, walkFrames);
		walk.setPlayMode(Animation.LOOP_PINGPONG);
		
		standSheet = new Texture(Gdx.files.internal("images/standing.png"));
		TextureRegion[][] tmp1 = TextureRegion.split(standSheet, standSheet.getWidth() / 2, standSheet.getHeight() / 1);
		standFrames = new TextureRegion[2 * 1];
		int index1 = 0;
		for(int i = 0; i < 1; i++)
			for(int j = 0; j < 2; j++){
				standFrames[index1++] = tmp1[i][j];
			}
		stand = new Animation(0.2f, standFrames);
		
		jumpSheet = new Texture(Gdx.files.internal("images/jumping.png"));
		TextureRegion[][] tmp2 = TextureRegion.split(jumpSheet, jumpSheet.getWidth() / 1, jumpSheet.getHeight() / 1);
		jumpFrames = new TextureRegion[1 * 1];
		int index2 = 0;
		for(int i = 0; i < 1; i++)
			for(int j = 0; j < 1; j++){
				jumpFrames[index2++] = tmp2[i][j];
			}
		jump = new Animation(0, jumpFrames);
		
		dinoSheet = new Texture(Gdx.files.internal("images/dino.png"));
		TextureRegion[][] tmp3 = TextureRegion.split(dinoSheet, dinoSheet.getWidth() / 2, dinoSheet.getHeight());
		dinoFrames = new TextureRegion[2];
		int index3 = 0;
		for(int i = 0; i < 1; i++)
			for(int j = 0; j < 2; j++){
				dinoFrames[index3++] = tmp3[i][j];
			}
		dino = new Animation(0.2f, dinoFrames);
		dino.setPlayMode(Animation.LOOP_PINGPONG);
		
		dinolineSheet = new Texture(Gdx.files.internal("images/dinoline.png"));
		TextureRegion[][] tmp5 = TextureRegion.split(dinolineSheet,dinolineSheet.getWidth() /2, dinolineSheet.getHeight());
		dinolineFrames = new TextureRegion[2];
		int index5 = 0;
		for(int i = 0; i<1; i++)
			for(int j = 0; j <2; j++)
				dinolineFrames[index5++] = tmp5[i][j];
		dinoline = new Animation(0.2f, dinolineFrames);
		dinoline.setPlayMode(Animation.LOOP_PINGPONG);
		
		heartSheet = new Texture(Gdx.files.internal("images/heart.png"));
		TextureRegion[][] tmp6 = TextureRegion.split(heartSheet,heartSheet.getWidth() /2, heartSheet.getHeight());
		heartFrames = new TextureRegion[2];
		int index6 = 0;
		for(int i = 0; i<1; i++)
			for(int j = 0; j <2; j++)
				heartFrames[index6++] = tmp6[i][j];
		heart = new Animation(0.2f, heartFrames);
		heart.setPlayMode(Animation.LOOP_PINGPONG);
		
		fireSheet = new Texture(Gdx.files.internal("images/fire.png"));
		TextureRegion[][] tmp4 = TextureRegion.split(fireSheet, fireSheet.getWidth() /4, fireSheet.getHeight());
		fireFrames = new TextureRegion[4*1];
		int index4 = 0;
		for(int i = 0; i<1; i++)
			for(int j = 0; j<4; j++){
				fireFrames[index4++] = tmp4[i][j];
			}
		fire = new Animation(0.2f, fireFrames);
		fire.setPlayMode(Animation.LOOP_PINGPONG);
		
				
	
		
		spriteBatch = new SpriteBatch();
		stateTime = 0f;
		
		
		Player.WIDTH = 1/16f * jumpSheet.getWidth();
		Player.HEIGHT =1/16f * jumpSheet.getHeight();
		Enemy.WIDTH = 1/16f * dinoSheet.getWidth()/2;
		Enemy.HEIGHT = 1/16f * dinoSheet.getHeight();
		map();
		
	    
		
		camera = new OrthographicCamera();
		camera.setToOrtho(false, 15	, 10);
		camera.update();
		
		endGegner = new Enemy();
		endGegner.position.set(477,5);
		gegner = new Enemy();
		gegner.position.set(1,5);
		spieler = new Player();
		spieler.position.set(15, 15);
		
		
		
		fireRect.add(a.getFireRect());
		fireRect.add(b.getFireRect());
		fireRect.add(c.getFireRect());
		fireRect.add(d.getFireRect());
		fireRect.add(e.getFireRect());
		fireRect.add(f.getFireRect());
		fireRect.add(g.getFireRect());
		fireRect.add(i.getFireRect());
		fireRect.add(j.getFireRect());
		fireRect.add(k.getFireRect());
		fireRect.add(m.getFireRect());
		fireRect.add(l.getFireRect());
		fireRect.add(n.getFireRect());
		fireRect.add(o.getFireRect());
		fireRect.add(p.getFireRect());
		fireRect.add(q.getFireRect());
		fireRect.add(r.getFireRect());
//		fireRect.add(s.getFireRect());
		fireRect.add(t.getFireRect());
		fireRect.add(u.getFireRect());
		fireRect.add(v.getFireRect());
		fireRect.add(w.getFireRect());
		fireRect.add(x.getFireRect());
//		fireRect.add(y.getFireRect());
//		fireRect.add(z.getFireRect());
		
		
	}
	
	@Override
	public void render(float delta) {
		switch (key) {
		case 1:
			Gdx.gl.glClearColor(0, 0.5f, 0.8f, 1);
			Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
			break;

		case 2:
			Gdx.gl.glClearColor(0.9f, 0.8f, 0.7f, 1);
			Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
			break;

		case 3:
			Gdx.gl.glClearColor(0f, 0f, 0.8f, 1);
			Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
			break;

		default:
			break;
		}
		
		if(spieler.position.x <= 450){
		if(lastTime >= 10){
			lastTime = 0;
			key++;
			if(key == 4)
				key = 1;
			map();
			
		}
		} 
		if(spieler.position.x > 450 && !firstTime2){
			key = 1;
			map();
			firstTime2 = true;
		}
		
		if(spieler.position.x <= 460){
         camera.position.x = spieler.position.x;

		} else {
			 
			if (camera.position.x <= 475)
				camera.position.x += delta * 2;
			if (camera.position.x >= 465 && !dinoAuto) {
				gegner.position.set(450, 5);
				
				dinoAuto = true;
				System.out.println("gegner wird zur�ckgesetzt");
			}
		}

		camera.update();
        
		if (spieler.position.x <= 460)
			updatePlayer(delta);

		
       
       
        
         
         stateTime += Gdx.graphics.getDeltaTime();
         lastTime += Gdx.graphics.getDeltaTime();
//         currentFrame = walk.getKeyFrame(stateTime, true);
         
         
         
         String s = String.valueOf(stateTime);
         renderer.setView(camera);
         renderer.render();
        
         
         
		switch (key) {
		case 1:
			updateDino(delta);
			renderDino(delta);
			renderDinoline(delta);
			break;
		case 2:
			updateFire(delta);
			renderFire(delta);
			break;
		case 3:
			break;

		default:
			break;
		}
		
		if(dinoAuto)
			endTime += Gdx.graphics.getDeltaTime();
		
		if(endTime >= 6)
			renderHeart(delta);
		

       
        
         spriteBatch.begin();
         
         if(endTime >= 6){
        	 
        	game.font.draw(spriteBatch, "hiccup 2 - The Hick Of Destiny", Gdx.graphics.getWidth()/2-100, Gdx.graphics.getHeight()-50);
        	
        	if(endTime >= 7)
        	game.font.draw(spriteBatch, "Made by Tiffi", Gdx.graphics.getWidth()/2-100, Gdx.graphics.getHeight()-70);
        	
        	
        	if(endTime >= 10)
        	game.font.draw(spriteBatch, "Still a better Love Story than Twilight", Gdx.graphics.getWidth()/2-100, Gdx.graphics.getHeight()-150);
         }
         
         if(hics){
 			game.font.draw(spriteBatch, "hics...", Gdx.graphics.getWidth()/2 +50, spieler.position.y*50 +30);
 			hicsTime += Gdx.graphics.getDeltaTime();
 			if(hicsTime > 3){
 				hics = false;
 				hicsTime = 0;
 				
 			}
 			
         }
         
//         game.font.draw(spriteBatch, s + " " + "Key: " + key , 100, 100);
//         game.font.draw(spriteBatch, ""+ endTime, 100, 140);
//         game.font.draw(spriteBatch,"y: " + spieler.position.y, 100, 80);
//         game.font.draw(spriteBatch,"x: " + spieler.position.x, 100, 120);
//         game.font.draw(spriteBatch,"x- dino: " + gegner.position.x, 100, 160);
     
         
         spriteBatch.end();
         
         
         renderPlayer(delta);
      
		
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
//		map.dispose();
	}
	
	
	
	private void updatePlayer(float deltaTime)
	{
		spieler.stateTime += deltaTime;
		
		if((Gdx.input.isKeyPressed(Keys.SPACE) || isTouched(0.75f, 1)) && spieler.grounded) {
			spieler.velocity.y += Player.JUMP_VELOCITY;
			spieler.state = Player.State.Jumping;
			jumpSound.play();
			spieler.grounded = false;
		}

		if(Gdx.input.isKeyPressed(Keys.LEFT) || Gdx.input.isKeyPressed(Keys.A) || isTouched(0, 0.25f)) {
			spieler.velocity.x = -Player.MAX_VELOCITY;
			if(spieler.grounded) spieler.state = Player.State.Walking;
			spieler.facesRight = false;
		}

		if(Gdx.input.isKeyPressed(Keys.RIGHT) || Gdx.input.isKeyPressed(Keys.D) || isTouched(0.25f, 0.5f)) {
			spieler.velocity.x = Player.MAX_VELOCITY;
			if(spieler.grounded) spieler.state = Player.State.Walking;
			spieler.facesRight = true;
		}
		
		spieler.velocity.add(0, gravity);
		
		if(Math.abs(spieler.velocity.x) > Player.MAX_VELOCITY) {
			spieler.velocity.x = Math.signum(spieler.velocity.x) * Player.MAX_VELOCITY;
		}
		
		
		if(Math.abs(spieler.velocity.x) < 1) {
			spieler.velocity.x = 0;
			if(spieler.grounded) spieler.state = Player.State.Standing;
		}
		
		spieler.velocity.scl(deltaTime);
		
		
		// collision detection x 
		Rectangle playerRect = rectPool.obtain();
		playerRect.set(spieler.position.x, spieler.position.y, Player.WIDTH*3/4, Player.HEIGHT);
		playerRectangle = rectPool.obtain();
		playerRectangle.set(spieler.position.x, spieler.position.y, Player.WIDTH*3/4, Player.HEIGHT);
		int startX, startY, endX, endY;
		
		if(spieler.velocity.x > 0){
			startX = endX = (int)(spieler.position.x + Player.WIDTH + spieler.velocity.x);
		}else{
			startX = endX = (int)(spieler.position.x + spieler.velocity.x);
		}
		startY = (int)spieler.position.y;
		endY = (int)(spieler.position.x + Player.HEIGHT);
		getTiles(startX, startY, endX, endY, tiles);
		playerRect.x += spieler.velocity.x;
		for(Rectangle tile : tiles) {
			if(playerRect.overlaps(tile)){
				spieler.velocity.x = 0;
				break;
			}
		}
		playerRect.x = spieler.position.x;
		
		// collision detection y
		if(spieler.velocity.y > 0){
			startY =endY = (int)(spieler.position.y + Player.HEIGHT + spieler.velocity.y);
		}else {
			startY = endY = (int)(spieler.position.y + spieler.velocity.y);
		}
		startX = (int)spieler.position.x;
		endX = (int)(spieler.position.x + Player.WIDTH);
		getTiles(startX, startY, endX, endY, tiles);
		playerRect.y += spieler.velocity.y;
		for(Rectangle tile : tiles) {
			if(playerRect.overlaps(tile)){
				if(spieler.velocity.y >0) {
					spieler.position.y = tile.y - Player.HEIGHT;
					TiledMapTileLayer layer = (TiledMapTileLayer)map.getLayers().get(1);
					layer.setCell((int)tile.x, (int)tile.y, null);
				} else {
					spieler.position.y = tile.y + tile.height;
					spieler.grounded = true;
				}
				spieler.velocity.y = 0;
				break;
			}
		}
		rectPool.free(playerRect);
		
		spieler.position.add(spieler.velocity);
		spieler.velocity.scl(1/deltaTime);
		
		spieler.velocity.x *=  Player.DAMPING;
		
		
		if(spieler.position.y <= 0){
			spieler.dead = true;
		}
		if(spieler.dead){
			
			deathSound.play();
			game.setScreen(new GameOverScreen(game));
            dispose();
		}
		
		
		
		
	}
	
	public void map(){
				
		switch (key) {
		case 1:
			map = new TmxMapLoader().load("data/world/level1/level1.tmx");
			renderer = new OrthogonalTiledMapRenderer(map, 1 / 16f);
			if (firstTime && spieler.position.x <= 400)
				gegner.position.set(spieler.position.x - 13, 5);
			timeSound.play();
			System.out.println("map gesetzt");
			firstTime = false;
			hics = true;
			break;
		case 2:
			map = new TmxMapLoader().load("data/world/level1/level2.tmx");
			renderer = new OrthogonalTiledMapRenderer(map, 1 / 16f);
			hics = true;
			firstTime = true;
			timeSound.play();
			break;
		case 3:

			map = new TmxMapLoader().load("data/world/level1/level3.tmx");
			renderer = new OrthogonalTiledMapRenderer(map, 1 / 16f);
			hics = true;
			firstTime = true;
			timeSound.play();
			break;

		default:
			break;
		}

		
		
	}
	
	
	private boolean isTouched(float startX, float endX) {
		for(int i = 0; i < 2; i++) {
			float x = Gdx.input.getX() / (float)Gdx.graphics.getWidth();
			if(Gdx.input.isTouched(i) && (x >= startX && x <= endX)) {
				return true;
			}
		}
		return false;
	}
	
	private void getTiles(int startX, int startY, int endX, int endY, Array<Rectangle> tiles){
		TiledMapTileLayer layer = (TiledMapTileLayer)map.getLayers().get(1);
		rectPool.freeAll(tiles);
		tiles.clear();
		for(int y = startY; y <= endY; y++){
			for(int x = startX; x <= endX; x++) {
				Cell cell = layer.getCell(x, y);
				if(cell != null) {
					Rectangle rect = rectPool.obtain();
					rect.set(x, y, 1, 1);
					
					tiles.add(rect);
				}
			}
		}
	}
	
	private void getTiles1(int startX, int startY, int endX, int endY, Array<Rectangle> tiles){
		TiledMapTileLayer layer = (TiledMapTileLayer)map.getLayers().get(0);
		rectPool.freeAll(tiles);
		tiles.clear();
		for(int y = startY; y <= endY; y++){
			for(int x = startX; x <= endX; x++) {
				Cell cell = layer.getCell(x, y);
				if(cell != null) {
					Rectangle rect = rectPool.obtain();
					rect.set(x, y, 1, 1);
					
					tiles.add(rect);
				}
			}
		}
	}
	
	
	private void renderPlayer(float deltaTime) {
		TextureRegion frame = null;
		
		switch(spieler.state) {
		case Standing: frame = stand.getKeyFrame(spieler.stateTime); break;
		case Walking: frame = walk.getKeyFrame(spieler.stateTime); break;
		case Jumping: frame = jump.getKeyFrame(spieler.stateTime); break;
		}
		
		SpriteBatch batch = renderer.getSpriteBatch();
		batch.begin();
		if(spieler.facesRight) {
			batch.draw(frame, spieler.position.x, spieler.position.y, Player.WIDTH, Player.HEIGHT);
		} else {
			batch.draw(frame, spieler.position.x + Player.WIDTH, spieler.position.y,-Player.WIDTH, Player.HEIGHT);
		}
		batch.end();
	}
	
	
	private void updateDino(float deltaTime)
	{
		gegner.stateTime += deltaTime;
		
		if(!dinoAuto){
		if(enemyJump && gegner.grounded) {
			gegner.velocity.y += Enemy.JUMP_VELOCITY;
			gegner.velocity.x = 50;
			gegner.state = Enemy.State.Jumping;
			gegner.grounded = false;
			enemyJump =false;
		}
		}
		
		if(spieler.position.x -1.5 < gegner.position.x && !spieler.grounded){
			enemyJump = true;
		}
			
		if (!dinoAuto) {
			if (spieler.position.x > gegner.position.x) {
				gegner.velocity.x = Enemy.MAX_VELOCITY;
				if (gegner.grounded)
					gegner.state = Enemy.State.Walking;
				gegner.facesRight = true;
			}
		}
		if(!dinoAuto && gegner.position.x >= 400){
			gegner.position.set(400, 7);
			
		}
		if (dinoAuto) {
			if (gegner.position.x <= 474){
				gegner.velocity.x = Enemy.MAX_VELOCITY;
				gegner.state = Enemy.State.Walking;
			}
		}
		
		
		if(!dinoAuto){
		if(spieler.position.x <= gegner.position.x){
			gegner.velocity.x = -Enemy.JUMP_VELOCITY;
			if(gegner.grounded) gegner.state = Enemy.State.Walking;
			gegner.facesRight = false;
		}
		}
		
		gegner.velocity.add(0, gravity);
		
		if(Math.abs(gegner.velocity.x) > Enemy.MAX_VELOCITY) {
			gegner.velocity.x = Math.signum(gegner.velocity.x) * Enemy.MAX_VELOCITY;
		}
		
		
		if(Math.abs(gegner.velocity.x) < 1) {
			gegner.velocity.x = 0;
			if(gegner.grounded) gegner.state = Enemy.State.Standing;
		}
		
		gegner.velocity.scl(deltaTime);
		
		
		// collision detection x 
		Rectangle EnemyRect = rectPool.obtain();
		EnemyRect.set(gegner.position.x, gegner.position.y, Enemy.WIDTH, Enemy.HEIGHT);
		int startX, startY, endX, endY;
		
		if(gegner.velocity.x > 0){
			startX = endX = (int)(gegner.position.x + Enemy.WIDTH + gegner.velocity.x);
		}else{
			startX = endX = (int)(gegner.position.x + gegner.velocity.x);
		}
		startY = (int)gegner.position.y;
		endY = (int)(gegner.position.x + Enemy.HEIGHT);
		getTiles1(startX, startY, endX, endY, tiles);
		EnemyRect.x += gegner.velocity.x;
		for(Rectangle tile : tiles) {
			if(EnemyRect.overlaps(tile)){
				gegner.velocity.x = 0;
				enemyJump = true;
				break;
			}
		}
		
		if(EnemyRect.overlaps(playerRectangle))
		{
			dinoSound.play();
			spieler.dead = true;
		}
		
		EnemyRect.x = gegner.position.x;
		
		// collision detection y
		if(gegner.velocity.y > 0){
			startY =endY = (int)(gegner.position.y + Enemy.HEIGHT + gegner.velocity.y);
		}else {
			startY = endY = (int)(gegner.position.y + gegner.velocity.y);
		}
		startX = (int)gegner.position.x;
		endX = (int)(gegner.position.x + Enemy.WIDTH);
		getTiles(startX, startY, endX, endY, tiles);
		EnemyRect.y += gegner.velocity.y;
		for(Rectangle tile : tiles) {
			if(EnemyRect.overlaps(tile)){
				if(gegner.velocity.y >0) {
					gegner.position.y = tile.y - Enemy.HEIGHT;
					TiledMapTileLayer layer = (TiledMapTileLayer)map.getLayers().get(0);
					layer.setCell((int)tile.x, (int)tile.y, null);
				} else {
					gegner.position.y = tile.y + tile.height;
					gegner.grounded = true;
				}
				gegner.velocity.y = 0;
				break;
			}
		}
		rectPool.free(EnemyRect);
		
		gegner.position.add(gegner.velocity);
		gegner.velocity.scl(1/deltaTime);
		
		gegner.velocity.x *=  Enemy.DAMPING;
		
		
		if(gegner.position.y <= 0){
			gegner.dead = true;
		}
		
		
		
		
	}
	
	
	private void renderDino(float deltaTime) {
		TextureRegion frame = null;
		
		switch(gegner.state) {
		case Standing: frame = dino.getKeyFrame(lastTime); break;
		case Walking: frame = dino.getKeyFrame(lastTime); break;
		case Jumping: frame = dino.getKeyFrame(lastTime); break;
		}
		
		SpriteBatch batch = renderer.getSpriteBatch();
		batch.begin();
		if(gegner.facesRight) {
			batch.draw(frame, gegner.position.x + Enemy.WIDTH, gegner.position.y,-Enemy.WIDTH*2, Enemy.HEIGHT*2);
		} else {
			
			batch.draw(frame, gegner.position.x, gegner.position.y, Enemy.WIDTH*2, Enemy.HEIGHT*2);
		}
		batch.end();
	}
	
	private void renderDinoline(float deltaTime) {
		TextureRegion frame = null;
		
		frame = dinoline.getKeyFrame(lastTime);
		
		
		
		SpriteBatch batch = renderer.getSpriteBatch();
		batch.begin();
		
			batch.draw(frame, 477, 3,Enemy.WIDTH*2, Enemy.HEIGHT*2);
		
		batch.end();
	}
	
	
	private void renderHeart(float deltaTime) {
		TextureRegion frame = null;
		
		frame = heart.getKeyFrame(lastTime);
		
		
		
		
		SpriteBatch batch = renderer.getSpriteBatch();
		batch.begin();
			
			
		batch.draw(frame, 475, 11, 470, 10, Player.WIDTH, Player.HEIGHT, 1, 1, 0.5f);
		
		if(endTime >= 7)
			batch.draw(frame, 476, 9, 470, 10, Player.WIDTH, Player.HEIGHT, 1, 1, 0.5f);
			
		batch.end();
	}
	
	private void updateFire(float deltaTime)
	{
		
		
		
		for(Rectangle fireTmp : fireRect){
			if(fireTmp.overlaps(playerRectangle))
				spieler.dead = true;
		}
		
		
		
		
		
		
	}
	
	private void renderFire(float deltaTime) {
		TextureRegion frame = null;
		
		
		frame = fire.getKeyFrame(spieler.stateTime);
		
		
		
		SpriteBatch batch = renderer.getSpriteBatch();
		batch.begin();
			batch.draw(frame, 104, 3,1, 1);
			batch.draw(frame, 109, 3,1, 1);
			batch.draw(frame, 114, 3,1, 1);
			batch.draw(frame, 121, 3,1, 1);
			batch.draw(frame, 140, 2,1, 1);
			batch.draw(frame, 141, 2,1, 1);
			batch.draw(frame, 142, 2,1, 1);			
			batch.draw(frame, 157, 2,1, 1);			
			batch.draw(frame, 160, 2,1, 1);			
			batch.draw(frame, 161, 2,1, 1);			
			batch.draw(frame, 167,2,1, 1);			
			batch.draw(frame, 182,6,1, 1);			
			batch.draw(frame, 204,3,1, 1);			
			batch.draw(frame, 228,7,1, 1);			
			batch.draw(frame, 238,3,1, 1);			
			batch.draw(frame, 250,5,2, 2);			
			batch.draw(frame, 276,3,1, 1);			
//			batch.draw(frame, 289,5,1, 1);			
			batch.draw(frame, 313,8,8, 8);			
			batch.draw(frame, 340,1,2, 2);			
			batch.draw(frame, 381,4,2, 2);			
			batch.draw(frame, 390,4,1, 1);			
			batch.draw(frame, 402,4,2, 2);			
				
		batch.end();
	
		
	}


}
























