package com.me.hickup;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.me.hickup.MainMenuScreen;

public class Hiccup extends Game {
    SpriteBatch batch;
    BitmapFont font;
    Sound music;
	
	@Override
	public void create() {		
		batch = new SpriteBatch();
		font = new BitmapFont();
		music = Gdx.audio.newSound(Gdx.files.internal("data/sounds/music.mp3"));
		music.loop(0.5f);
	
		this.setScreen(new IntroScreen(this));
		
	}



	@Override
	public void render() {		
		super.render();
	}
	
	@Override
	public void dispose() {
	    batch.dispose();
        font.dispose();
	}

}
