package com.me.hickup;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class Main {
	public static void main(String[] args) {
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.title = "Hiccup 2 - The Hic of Destiny";
		cfg.width = 800;
		cfg.height = 480;
		cfg.resizable = false;
		
		new LwjglApplication(new Hiccup(), cfg);
	}
}
